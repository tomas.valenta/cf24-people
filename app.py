import flask
import time
import obsws_python as obs

# pass conn info if not in config.toml
cl = obs.ReqClient(host='localhost', port=4455, password='6BKKDvrrDgzAqqlt', timeout=3)

app = flask.Flask(__name__)

PEOPLE = [
    {
        "name": "Ivan Bartoš",
        "position": "Předseda strany",
        "image": "people/Ivan_Bartos.png",
    },
    {
        "name": "Marcel Kolaja",
        "position": "Europoslanec, lídr kandidátky",
        "image": "people/Marcel_Kolaja.png",
    },
    {
        "name": "Markéta Gregorová",
        "position": "Europoslankyně",
        "image": "people/Marketa_gregorova.png",
    },
    {
        "name": "Mikuláš Peksa",
        "position": "Europoslanec",
        "image": "people/Mikulas_Peksa.png",
    },
    {
        "name": "Zuzana Klusová",
        "position": "Karvinská politička a aktivistka",
        "image": "people/Zuzana_klusova.png",
    },
]

@app.route("/", methods=["GET", "POST"])
def switch():
    if flask.request.method == "POST":
        cl.set_current_program_scene("Camera")

        time.sleep(0.3)

        with open("name.txt", "w") as name_file:
            name_file.write(flask.request.form["name"])

        with open("position.txt", "w") as position_file:
            position_file.write(flask.request.form["position"])

        for person in PEOPLE:
            if person["name"] == flask.request.form["name"]:
                with open(person["image"], "rb") as source_image_file:
                    with open("profile.png", "wb") as destination_image_file:
                        destination_image_file.write(source_image_file.read())

        time.sleep(3)

        if flask.request.form["name"] != "":
            cl.set_current_program_scene("Camera with person text")
        else:
            cl.set_current_program_scene("Camera")

    with open("name.txt", "r") as name_file:
        name = name_file.read()

    with open("position.txt", "r") as position_file:
        position = position_file.read()

    return flask.render_template("index.html", people=PEOPLE, name=name, position=position)
